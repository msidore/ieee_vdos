#!/usr/bin/python
# -*- coding: utf-8 -*-
from modeller import *
from modeller.scripts import complete_pdb
from argparse import ArgumentParser

"""

Trying to be as simple as possible. This is a small script to add missing atoms in a .pdb structure.
This probably won't work if whole residues are missing (?). I think it will actually work with missing residues.

Usage: ./add_missing_atoms.py -f 3v03.pdb -o 3v03_fixed.pdb

"""

######################## Parsing stuff ########################

parser = ArgumentParser(description=""" Trying to be as simple as possible. This is a small script to add missing atoms in a .pdb structure.
This probably won't work if whole residues are missing (?). I think it will actually work with missing residues.""")

# Named arguments
parser.add_argument("-f", "--file", help="The name of the .pdb input file")
parser.add_argument("-o", "--output", help="The of the .pdb output file")

args = parser.parse_args()

######################## Functions and miscellaneous ########################

# Input and output
pdb_in = args.file
pdb_out = args.output

######################## Main ########################

# Safeguard
log.verbose()

# Create the modeller environment and specify where the input files are
env = environ()
env.io.atom_files_directory = ['.']

# Read the topology and parameters
env.libs.topology.read(file='$(LIB)/top_heav.lib')
env.libs.parameters.read(file='$(LIB)/par.lib')

# Tell it to keep hetatm and water residues
env.io.hetatm = True
env.io.water = True

# The thing to complete the pdb - does it add missing residues ? TBT
mdl = complete_pdb(env, pdb_in, special_patches=None, transfer_res_num=False, model_segment=None, patch_default=True) 

# Write the output ?
mdl.write(pdb_out, model_format='PDB', no_ter=False) 















